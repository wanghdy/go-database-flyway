package go_database_flyway

import (
	"database/sql"
	"errors"
	"gitee.com/wanghdy/go-database-flyway/flyway"
	"log"
)

const (
	Mysql = "mysql"
)

func Start(typ string, db *sql.DB) error {
	if typ != Mysql {
		log.Println("database type fail:" + typ)
		return errors.New("database type fail:" + typ)
	}
	flyway.Entrance(db)
	return nil
}
