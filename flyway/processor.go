package flyway

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

type SqlList struct {
	Path SqlPath
	Name SqlName
}

type SqlPath struct {
	string
}

type SqlName struct {
	string
}

const (
	DefaultPath   = "resources/flyway/sql"
	DefaultPrefix = "V"
	DefaultSplit  = "__"
	DefaultSuffix = ".sql"
)

// ReadSqlList Traversal folder
func (receiver SqlList) ReadSqlList() []SqlList {
	list := make([]SqlList, 0)
	// 遍历当前目录下的所有文件，不遍历子目录
	err := filepath.Walk(DefaultPath, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		// 如果是子目录，就跳过
		if info.IsDir() {
			return nil
		}
		err = receiver.verifySqlFile(info.Name())
		if err != nil {
			return err
		}

		list = append(list, SqlList{
			Path: SqlPath{path},
			Name: SqlName{info.Name()},
		})
		return nil
	})
	if err != nil {
		PrintStack(err)
	}
	return list
}

//verifySqlFile
//Vversion__description.sql
func (receiver SqlList) verifySqlFile(filename string) error {
	err := errors.New(fmt.Sprintf("invalid filename: %s", filename))
	if !strings.HasSuffix(filename, DefaultSuffix) {
		return err
	}
	fields := strings.Split(filename, DefaultSplit)
	if len(fields) != 2 {
		return err
	}
	if !strings.Contains(fields[0], DefaultPrefix) {
		return err
	}
	if len(strings.ReplaceAll(fields[0], DefaultPrefix, "")) == 0 {
		return err
	}
	_, err = strconv.Atoi(strings.ReplaceAll(strings.ReplaceAll(fields[0], DefaultPrefix, ""), ".", ""))
	if err != nil {
		return err
	}
	return nil
}

func (receiver SqlPath) GetFile() string {
	bytes, err := ioutil.ReadFile(receiver.string)
	if err != nil {
		PrintStack(err)
	}
	return string(bytes)
}

func (receiver SqlList) ToModel() FlywayRecords {
	fields := strings.Split(receiver.Name.string, DefaultSplit)
	version := strings.ReplaceAll(fields[0], DefaultPrefix, "")
	return FlywayRecords{
		Description: strings.ReplaceAll(fields[0], DefaultSuffix, ""),
		Script:      receiver.Name.string,
		Version:     version,
		CreateTime:  int(time.Now().Unix()),
		Path:        receiver.Path,
		Name:        receiver.Name,
	}
}
