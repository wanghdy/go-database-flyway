package flyway

import (
	"database/sql"
	"fmt"
	"log"
	"strconv"
	"strings"
	"sort"
)

func Entrance(db *sql.DB) {
	defer func() {
		if er := recover(); er != nil {
			log.Println(er)
			log.Println("flyway interruption...")
		}
	}()
	log.Println("flyway start...")
	//判断有没有记录表，没有就添加
	preparation(db)
	//获取增量记录
	incremental := getIncremental(db)
	//执行增量sql
	ExcSql(db, incremental)
	log.Println("flyway end...")
}

//准备工作
func preparation(db *sql.DB) {
	// 查询数据库名称
	var databaseName string
	err := db.QueryRow("SELECT DATABASE()").Scan(&databaseName)
	if err != nil {
		log.Println(err)
		PrintStack(err)
	}
	//
	var exists bool
	query := fmt.Sprintf("SELECT EXISTS(SELECT 1 FROM information_schema.tables WHERE table_schema = '%s' AND table_name = '%s')", databaseName, TableName())
	err = db.QueryRow(query).Scan(&exists)
	if err != nil {
		log.Println(err)
		PrintStack(err)
	}
	if !exists {
		log.Println("Initializes the flyway table")
		// 执行建表语句
		_, err = db.Exec(CreateSql())
		if err != nil {
			log.Println(err)
			PrintStack(err)
		}
	}
}

//获取需要增量的sql
func getIncremental(db *sql.DB) []FlywayRecords {
	oldVersion := queryFlywayVersion(db)
	list := FlywayRecords{}.GetNewFlywayModels()
	var incrementalList []FlywayRecords
	for _, item := range list {
		//compareVersion(v1, v2) < 0 即 v1 < v2
		if compareVersion(oldVersion, item.Version) < 0 {
			incrementalList = append(incrementalList, item)
		}
	}
	sort.Slice(incrementalList, func(i, j int) bool {
		return compareVersion(incrementalList[i].Version, incrementalList[j].Version) < 0
	})
	return incrementalList
}

func queryFlywayVersion(db *sql.DB) string {
	var count int
	err := db.QueryRow(fmt.Sprintf("SELECT count(*) FROM %s", TableName())).Scan(&count)
	if err != nil {
		log.Println(err)
		PrintStack(err)
	}
	if count == 0 {
		return ""
	}
	// 执行查询并将结果保存
	var version string
	err = db.QueryRow(fmt.Sprintf("SELECT `version` FROM %s ORDER BY id DESC LIMIT 1", TableName())).Scan(&version)
	if err != nil {
		log.Println(err)
		PrintStack(err)
	}
	return version
}

func ExcSql(db *sql.DB, incremental []FlywayRecords) {
	//开启事务
	tx, err := db.Begin()
	if err != nil {
		PrintStack(err)
	}
	for _, record := range incremental {
		//执行sql
		fileSql := record.GetFileSql()
		for _, fsql := range fileSql {
			_, err := tx.Exec(fsql)
			if err != nil {
				tx.Rollback()
				PrintStack(err)
			}
		}
		//插入记录表
		_, err := tx.Exec(InsertSql(), record.Description, record.Script, record.Version, record.CreateTime)
		if err != nil {
			tx.Rollback()
			PrintStack(err)
		}
		log.Println("flyway update data success " + record.Name.string)
	}
	//提交事务
	tx.Commit()
}

func compareVersion(version1, version2 string) int {
	parts1 := strings.Split(version1, ".")
	parts2 := strings.Split(version2, ".")

	len1, len2 := len(parts1), len(parts2)
	maxLen := len1
	if len2 > maxLen {
		maxLen = len2
	}

	for i := 0; i < maxLen; i++ {
		var num1, num2 int
		if i < len1 {
			num1, _ = strconv.Atoi(parts1[i])
		}
		if i < len2 {
			num2, _ = strconv.Atoi(parts2[i])
		}

		if num1 < num2 {
			return -1
		} else if num1 > num2 {
			return 1
		}
	}

	return 0
}
