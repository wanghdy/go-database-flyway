package flyway

import (
	"log"
	"runtime"
)

func PrintStack(err error) {
	// 获取当前堆栈信息
	buf := make([]byte, 1024)
	n := runtime.Stack(buf, false)
	// 打印堆栈信息
	log.Printf("Stack trace:\n%s\n", buf[:n])
	panic(err)
}
