package flyway

type FlywayRecords struct {
	Id          int
	Description string
	Script      string
	Version     string
	CreateTime  int
	Path        SqlPath
	Name        SqlName
}

func TableName() string {
	return "flyway_records"
}

func CreateSql() string {
	return "CREATE TABLE `flyway_records` ( \t`id` INT NOT NULL AUTO_INCREMENT, \t`description` VARCHAR ( 100 ) NULL DEFAULT NULL, \t`script` VARCHAR ( 100 ) NULL DEFAULT NULL, \t`version` VARCHAR ( 100 ) NULL DEFAULT NULL, \t`create_time` BIGINT NULL DEFAULT NULL, \tPRIMARY KEY ( `id` ) USING BTREE );"
}
func InsertSql() string {
	return "INSERT INTO flyway_records(`description`, `script`, `version`, `create_time`)VALUES(?,?,?,?)"
}

func (receiver FlywayRecords) GetNewFlywayModels() []FlywayRecords {
	list := SqlList{}.ReadSqlList()
	var records []FlywayRecords
	for _, sqlList := range list {
		model := sqlList.ToModel()
		records = append(records, model)
	}
	return records
}

func (receiver FlywayRecords) GetOldFlywayModels() ([]FlywayRecords, error) {
	return nil, nil
}

func (receiver FlywayRecords) ExistsTable() (bool, error) {
	return false, nil
}

func (receiver FlywayRecords) GetFileSql() []string {
	return SplitSQLStatements(receiver.Path.GetFile())
}

func SplitSQLStatements(sqlString string) []string {
	var sqlStatements []string
	var currentStatement string
	insideSingleQuotes := false

	for i, char := range sqlString {
		currentStatement += string(char)

		if char == '\'' {
			insideSingleQuotes = !insideSingleQuotes
		}

		if char == ';' && !insideSingleQuotes {
			// 如果分号不在单引号内，则认为是SQL语句结束
			sqlStatements = append(sqlStatements, currentStatement)
			currentStatement = ""
		} else if i == len(sqlString)-1 {
			// 处理最后一个SQL语句
			sqlStatements = append(sqlStatements, currentStatement)
		}
	}

	return sqlStatements
}
