# database-flyway 数据库脚本自动升级

发行版 **v0.0.1**  
当前仅支持 **mysql** 数据库

## 功能介绍

1. 自动读取数据库脚本执行
2. 记录执行的脚本记录

## 使用方法

#### 1.首先下载 **go-database-flyway**

    go get gitee.com/wanghdy/go-database-flyway@v0.0.1

#### 2.将Sql脚本放到项目根目录下的 **resource/flyway/sql/**   
SQL脚本文件需要遵守命名规则，V版本号__描述.sql  
V、版本号、英文双下划线、描述、.sql

例如

    项目A
       |
       |——main.go
       |——resources
          |——flyway
            |——sql
              |——V1.0.0__init.sql
              |——V1.0.1__insert.sql


#### 3.程序启动时执行 **Start** 方法

    import (
	"database/sql"
	flyway "gitee.com/wanghdy/go-database-flyway"
	_ "github.com/go-sql-driver/mysql"
	"log"
    )

    func main() {
      //连接数据库（如果是gorm等连接也同样将*DB传入Start方法）
      db, err := sql.Open("mysql", "root:root@tcp(localhost:3306)/flyway_test")
      if err != nil {
        log.Println(err)
        return
      }
      //执行自动更新
      flyway.Start(flyway.Mysql, db)
    }

启动成功控制台输出

    2023/10/18 11:26:29 flyway start...
    2023/10/18 11:26:29 Initializes the flyway table
    2023/10/18 11:26:29 flyway update data success V1.0.0__init.sql
    2023/10/18 11:26:29 flyway update data success V1.0.1__insert.sql
    2023/10/18 11:26:29 flyway end...

#### 4.查看数据库

成功生成记录表 **flyway_records**  
![img.png](img.png)

sql目录下的两个sql文件也执行成功  
![img_1.png](img_1.png)

后续升级sql,增量在sql目录下增加sql脚本即可,需要注意的是版本号需要增量